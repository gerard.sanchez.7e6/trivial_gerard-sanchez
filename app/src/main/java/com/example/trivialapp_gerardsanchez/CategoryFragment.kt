package com.example.trivialapp_gerardsanchez

import android.os.Bundle

import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.trivialapp_gerardsanchez.databinding.FragmentCategoryBinding


class CategoryFragment : Fragment() {
    companion object{
        var category = 0

    }
    private lateinit var binding: FragmentCategoryBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View {
        binding = FragmentCategoryBinding.inflate(layoutInflater)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.buttonReturn.setOnClickListener {
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, MenuFragment())
                setReorderingAllowed(true)
                commit()
            }
        }
        binding.buttonComenzar.setOnClickListener {
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, GameFragment())
                commit()
            }
        }
        binding.imgCiencia.setOnClickListener {
            Toast.makeText(activity,"Has escogido CIENCIA", Toast.LENGTH_SHORT).show()
            binding.buttonComenzar.visibility = view.visibility
            binding.buttonComenzar.isEnabled = true
            category = 0
        }
        binding.imgDeportes.setOnClickListener {
            Toast.makeText(activity,"Has escogido DEPORTES", Toast.LENGTH_SHORT).show()
            binding.buttonComenzar.visibility = view.visibility
            binding.buttonComenzar.isEnabled = true
            category = 1
        }
        binding.imgArte.setOnClickListener {
            Toast.makeText(activity,"Has escogido ARTE", Toast.LENGTH_SHORT).show()
            binding.buttonComenzar.visibility = view.visibility
            binding.buttonComenzar.isEnabled = true
            category = 2
        }
        binding.imgGeografia.setOnClickListener {
            Toast.makeText(activity,"Has escogido GEOGRAFÍA", Toast.LENGTH_SHORT).show()
            binding.buttonComenzar.visibility = view.visibility
            binding.buttonComenzar.isEnabled = true
            category = 3
        }
        binding.imgHistoria.setOnClickListener {
            Toast.makeText(activity,"Has escogido HISTORIA", Toast.LENGTH_SHORT).show()
            binding.buttonComenzar.visibility = view.visibility
            binding.buttonComenzar.isEnabled = true
            category = 4
        }
        binding.imgEntretenimiento.setOnClickListener {
            Toast.makeText(activity,"Has escogido ENTRETENIMIENTO", Toast.LENGTH_SHORT).show()
            binding.buttonComenzar.visibility = view.visibility
            binding.buttonComenzar.isEnabled = true
            category = 5
        }

    }
}