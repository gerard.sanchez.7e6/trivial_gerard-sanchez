package com.example.trivialapp_gerardsanchez

object questions {
    val preguntasCiencia: MutableList<MutableList<String>> = mutableListOf(
        mutableListOf(
            "¿En qué mes el Sol está más cerca de la Tierra? ",
            "f_Marzo",
            "c_Enero",
            "f_Julio",
            "f_Octubre"
        ),
        mutableListOf(
            "¿Cómo se llamaba la perra de la URSS que se convirtió en el primer ser vivo en viajar al espacio?",
            "f_Daria",
            "f_Muschka",
            "c_Laika",
            "f_Lassie"
        ),
        mutableListOf(
            "¿En que parte del cuerpo está el astrálago?",
            "f_Muñeca",
            "f_Rodilla",
            "f_Codo",
            "c_Tobillo"
        ),
        mutableListOf(
            "El aparato digestivo está formado por la boca, faringe, esófago, intestino delgado, intestino grueso y:",
            "c_Estómago",
            "f_Hígado",
            "f_Corazón",
            "f_Pulmón"
        ),
        mutableListOf(
            "¿Cómo se llaman las crias del águila?",
            "f_Aguiluño",
            "c_Aguilucho",
            "f_Águila",
            "f_Aguileño"
        ),
        mutableListOf(
            "¿Cuàl de los siguientes materiales es magnético de forma natural?",
            "c_Níquel",
            "f_Titanio",
            "f_Plomo",
            "f_Aluminio"
        ),
        mutableListOf(
            "¿Cómo se denomina al estudio científico relacionado con los océanos?",
            "f_Ciencia del mar",
            "f_Biociencia",
            "c_Oceanografía",
            "f_Biologia marina"
        ),
        mutableListOf(
            "¿Cuál de los siguientes osos no es en realidad un oso?",
            "f_Oso panda",
            "f_Oso blanco",
            "c_Oso hormiguero",
            "f_Oso pardo"
        ),
        mutableListOf(
            "Los sonidos que emiten los patos se llaman...",
            "f_Canto",
            "f_Cuacareo",
            "f_Cacareo",
            "c_Graznido"
        ),
        mutableListOf(
            "¿Qué químico y biólogo francés es considerado el fundador de la estereoquímica?",
            "f_Jules Rimet",
            "c_Louis Pasteur",
            "f_Samuel Umtiti",
            "f_Robert William Thompson"
        ),
        mutableListOf(
            "Las ranas pertenecen al grupo de los...",
            "f_Peces",
            "f_Reptiles",
            "f_Mamiferos",
            "c_Anfibios"
        ),
        mutableListOf(
            "¿Qué almacena el camello en sus jorobas?",
            "f_Agua",
            "c_Grasa",
            "f_Azúcar",
            "f_Nada"
        ),
        mutableListOf(
            "¿Cuántos números primos pares hay?",
            "f_Infinitos",
            "c_Uno",
            "f_Dos",
            "f_Ninguno"
        ),
        mutableListOf(
            "¿Cómo se llama el cambio de estado de líquido a gaseoso?",
            "c_Vaporización",
            "f_Sublimación",
            "f_Difusión",
            "f_Gasificación"
        ),
        mutableListOf(
            "¿Cuál es el símbolo del Sodio?",
            "f_Sd",
            "c_Na",
            "f_So",
            "f_S"
        )

    )

    val preguntasDeportes: MutableList<MutableList<String>> = mutableListOf(
        mutableListOf(
            "¿Qué selección de fútbol ganó la Copa del Mundo en 2006? ",
            "f_Brasil",
            "c_Italia",
            "f_Alemania",
            "f_Francia"
        ),
        mutableListOf(
            "En hockey sobre patines, ¿qué targeta te deja fuera del campo por unos minutos?",
            "f_Roja",
            "f_Amarilla",
            "c_Azul",
            "f_Negra"
        ),
        mutableListOf(
            "¿Quién ganó el Balón de Oro 2013?",
            "f_Andrés Iniesta",
            "f_Lionel Messi",
            "f_Iker Casillas",
            "c_Cristiano Ronaldo"
        ),
        mutableListOf(
            "¿Qué pais ha conseguido más medallas de oro en la modalidad de béisbol en los Juegos Olímpicos?",
            "c_Cuba",
            "f_Japón",
            "f_Estados Unidos",
            "f_Corea del Sur"
        ),
        mutableListOf(
            "¿Con qué deporte relacionarías a Michael Phelps?",
            "f_Equitación",
            "c_Natación",
            "f_Atletismo",
            "f_Ciclismo"
        ),
        mutableListOf(
            "¿De dónde es originario el waterpolo",
            "f_Israel",
            "c_Reino Unido",
            "f_Montenegro",
            "f_Estados Unidos"
        ),
        mutableListOf(
            "¿De qué ciudad es el equipo de fútbol Everton?",
            "f_Manchester",
            "c_Liverpool",
            "f_Londres",
            "f_Birmingham"
        ),
        mutableListOf(
            "¿Qué equipo ha ganado la Bundesliga alemana 2017-2018?",
            "f_Schalke 04",
            "c_Bayern de Munich",
            "f_RB Leipzig",
            "f_Borussia Dortmund"
        ),
        mutableListOf(
            "¿Cuál fue la primera selección no europea ni americana que participó en un Mundial de fútbol?",
            "f_Sudán",
            "f_Namibia",
            "f_Vietnam",
            "c_Egipto"
        ),
        mutableListOf(
            "¿Cómo se llamaba el estadio del Real Madrid antes de llamarse Santiago Bernabeu?",
            "f_Alfredo Di Stéfano",
            "f_La Castellana",
            "c_Chamartín",
            "f_Siempre ha tenido ese nombre"
        ),
        mutableListOf(
            "Las ranas pertenecen al grupo de los...",
            "f_Arquero",
            "f_Guardameta",
            "f_Cancerbero",
            "c_Las tres son correctas"
        ),
        mutableListOf(
            "¿En qué país nació el piloto de automovilismo Nicky Lauda?",
            "f_Alemamia",
            "c_Austria",
            "f_Suiza",
            "f_Liechtenstein"
        ),
        mutableListOf(
            "¿De qué nacionalidad es el futbolista Zlatan Ibrahimovic?",
            "f_Inglesa",
            "c_Sueca",
            "f_Suiza",
            "f_Escocesa"
        ),
        mutableListOf(
            "¿Qué marca de coches pasó de los rallies a la F1 en 1999?",
            "f_Renault",
            "f_Skoda",
            "f_Saab",
            "c_Toyota"
        ),
        mutableListOf(
            "¿En qué año se celebraron los Juegos Olímpicos en Barcelona?",
            "c_1992",
            "f_1982",
            "f_2002",
            "f_1986"
        ),
    )
    val preguntasArte: MutableList<MutableList<String>> = mutableListOf(
        mutableListOf(
            "Pierre August Renoir perteneció al género pictórico de:",
            "f_Cubismo",
            "c_Impresionismo",
            "f_Realismo",
            "f_Fauvismo"
        ),
        mutableListOf(
            "¿Quién fue la mujer más hermosa entre las mortales según la mitología griega?",
            "f_Némesis",
            "f_Hermione",
            "c_Helena de Troya",
            "f_Afrodita"
        ),
        mutableListOf(
            "¿Quién escribió Heidi en 1880?",
            "f_María Edgeworth",
            "f_C.S Lewis",
            "f_Anna Sewel",
            "c_Johanna Spyri"
        ),
        mutableListOf(
            "¿Cuál es el nombre del barco ballenero en 'Moby Dick'?",
            "c_Pequod",
            "f_Nautilus",
            "f_Essex",
            "f_Boston"
        ),
        mutableListOf(
            "¿Qué escritor español firmaba como Fígaro",
            "f_Machado",
            "c_Larra",
            "f_Quevedo",
            "f_Lorca"
        ),
        mutableListOf(
            "¿Dónde está el museo más visitado del mundo?",
            "c_París",
            "f_Madrid",
            "f_Nueva York",
            "f_Moscú"
        ),
        mutableListOf(
            "¿Qué comía la boa del cuento 'El Principito'?",
            "f_Un ratón",
            "f_Un huevo",
            "c_Un elefante",
            "f_Una lechuza"
        ),
        mutableListOf(
            "¿Cuál de los siguientes autores no pertenece al siglo XIX?",
            "f_Leopoldo Alas Clarín",
            "f_Emilia Pardo Bazán",
            "f_José Zorrilla",
            "c_Garcilaso de la Vega"
        ),
        mutableListOf(
            "¿Qué seres de la mitologia griega eran gigantes con un solo ojo en el medio de la frente?",
            "f_Faunos",
            "c_Cíclopes",
            "f_Centauros",
            "f_Minotauros"
        ),
        mutableListOf(
            "¿De dónde era el poeta Rafael Alberti?",
            "f_Bilbao",
            "f_Murcia",
            "c_Puerto de Santa Maria",
            "f_Verona"
        ),
        mutableListOf(
            "¿Cuál era el segundo apellido de Góngora?",
            "c_Argote",
            "f_Gilabert",
            "f_Celaya",
            "f_Hernández"
        ),
        mutableListOf(
            "¿En qué museo se encuetra 'El caballero de la mano en el pecho' de El Greco?",
            "c_Museo del Prado",
            "f_Museo del Louvre",
            "f_Museo de Santa Sofía",
            "f_Museo Thyssen"
        ),
        mutableListOf(
            "¿A qué famoso pintor le gustaban los girasoles?",
            "f_Joan Miró",
            "f_Pablo Picasso",
            "f_Salvador Dalí",
            "c_Vicent Van Gogh"
        ),
        mutableListOf(
            "¿Cuál es el instrumento musical típico de la Semana Santa de Sevilla?",
            "f_Triángulo",
            "f_Violín",
            "f_Cuerno",
            "c_Corneta"
        ),
        mutableListOf(
            "¿Cuál era el verdadero nombre del filósofo griego Platón?",
            "f_Platón",
            "f_Plutarco",
            "c_Aristocles",
            "f_Aristarco"
        ),
    )
    val preguntasGeografia: MutableList<MutableList<String>> = mutableListOf(
        mutableListOf(
            "¿De qué continente forma parte Oriente Medio?",
            "f_Europa",
            "c_Asia",
            "f_Oceanía",
            "f_África"
        ),
        mutableListOf(
            "¿Dónde se encuentra la 'Costa Cálida'?",
            "f_Gran Canaria",
            "f_Alicante",
            "c_Murcia",
            "f_Tenerife"
        ),
        mutableListOf(
            "¿En qué continente se encuentra el volcán Kilimanjaro?",
            "f_Asia",
            "f_América",
            "f_Europa",
            "c_África"
        ),
        mutableListOf(
            "¿Cuál es el puerto marítimo más importante dde Europa en el turismo de cruceros?",
            "c_Barcelona",
            "f_Venecia",
            "f_Génova",
            "f_Marsella"
        ),
        mutableListOf(
            "¿Cuál es el idioma oficial de Letonia?",
            "f_Inglés",
            "c_Letón",
            "f_Alemán",
            "f_Portugués"
        ),
        mutableListOf(
            "¿Con qué país limita al este de Montenegro?",
            "c_Serbia",
            "f_Egipto",
            "f_Turquía",
            "f_Grecia"
        ),
        mutableListOf(
            "¿Cuál es la capital de Portugal?",
            "f_Oporto",
            "c_Lisboa",
            "f_Braga",
            "f_Coímbra"
        ),
        mutableListOf(
            "¿Cuál de los siguientes idiomas NO es lengua oficial en Bélgica?",
            "f_Francés",
            "c_Belga",
            "f_Neerlandés",
            "f_Alemán"
        ),
        mutableListOf(
            "¿En qué país están los fiordos más famosos?",
            "f_Suecia",
            "f_Finlandia",
            "c_Noruega",
            "f_Dinamarca"
        ),
        mutableListOf(
            "¿Dónde se encuentran los famosos Jameos del Agua?",
            "c_Lanzarote",
            "f_Hawai",
            "f_Almería",
            "f_Tenerife"
        ),
        mutableListOf(
            "¿A qué pais pertenece la región de Kamchatka?",
            "f_Japón",
            "f_China",
            "f_Corea",
            "c_Rusia"
        ),
        mutableListOf(
            "¿En qué país latinoamericano se encuentra el estado de Nayarit?",
            "f_Brasil",
            "f_Bolivia",
            "c_México",
            "f_Perú"
        ),
        mutableListOf(
            "¿Calatayud pertenece a la provincia de?",
            "f_Guadalajara",
            "c_Zaragoza",
            "f_Teruel",
            "f_Huesca"
        ),
        mutableListOf(
            "¿Cuál de los siguientes 'ochomiles' no se encuentra en la cordillera del Himalaya?",
            "c_K2",
            "f_Todos son del Himalaya",
            "f_Everest",
            "f_Kanchenjunga"
        ),
        mutableListOf(
            "¿Cuál es la capital de Jordania?",
            "f_Lima",
            "c_Ammán",
            "f_Santo Domingo",
            "f_Sucre"
        )
    )
    val preguntasHistoria: MutableList<MutableList<String>> = mutableListOf(
        mutableListOf(
            "¿Qúe nombre recibía la ciudad de Almuñecar en la época romana?",
            "f_Rusadir",
            "c_Sexi",
            "f_Leiria",
            "f_Almuñeque"
        ),
        mutableListOf(
            "¿Qué presidente de los Estados Unidos fue actor?",
            "f_Jimmy Carter",
            "f_Arnold Scharzenegger",
            "c_Ronald Reagan",
            "f_Bill Clinton"
        ),
        mutableListOf(
            "¿Quién venció en la Guerra de los Cien Años?",
            "f_Inglaterra",
            "f_Italia",
            "f_Castilla",
            "c_Francia"
        ),
        mutableListOf(
            "¿Cómo se llamó el primer lider de los bolcheviques?",
            "c_Lenin",
            "f_Stalin",
            "f_Trosky",
            "f_Nicolás II"
        ),
        mutableListOf(
            "¿Qué partido politico ganó las elecciones generales en 2011 en España?",
            "c_PP",
            "f_IU",
            "f_UPyD",
            "f_PSOE"
        ),
        mutableListOf(
            "¿Quien sucedió a Julio César convirtiendose en el primer emperador de Roma?",
            "f_Calígula",
            "f_Tiberio",
            "c_Octavio Augusto",
            "f_Nerón"
         ),
        mutableListOf(
            "¿Cuál de estos presidentes de los EStados Unidos murió asesinado?",
            "f_Ulysses S.Grant",
            "c_William Mckinley",
            "f_Grover Cleveland",
            "f_James Monroe"
        ),
        mutableListOf(
            "¿En qué periodo histórico se reconoció por primera vez el deracho al divorcio en España?",
            "f_Dictadura del general Franco",
            "f_Dictadura de Primo de Rivera",
            "f_Gobierno de Adolfo Suárez",
            "c_Segunda República"
        ),
        mutableListOf(
            "¿Cómo se apodó popularmente  a la Constitución de Cádiz de 1812?",
            "f_La Española",
            "f_La Gloriosa",
            "c_La Pepa",
            "f_La Veneno"
        ),
        mutableListOf(
            "¿Cuál fue la base de la economía azteca?",
            "c_La agricultura",
            "f_El trueque",
            "f_La navegación",
            "f_La mineria"
        ),
        mutableListOf(
            "¿Entre qué años reino Carlos IV en España?",
            "f_1828 y 1848",
            "f_1808 y 1828",
            "c_1788 y 1808",
            "f_1768 y 1788"
        ),
        mutableListOf(
            "¿Cuál era el nombre del conquistador español que dio fin al Imperio Inca?",
            "f_Franciscode Orellana",
            "f_Cabeza de Vaca",
            "f_Diego Velázquez",
            "c_Francisco Pizarro"
        ),
        mutableListOf(
            "¿En qué país se originó el budismo?",
            "c_India",
            "f_China",
            "f_Birmania",
            "f_Japón"
        ),
        mutableListOf(
            "¿Qué compró Estados Unidos a Rusia por dos centavos?",
            "f_Polo Norte",
            "c_Alaska",
            "f_Polo Sur",
            "f_Hawaii"
        ),
        mutableListOf(
            "¿Qué 2 dictadores aliados murieron el mismo año (1945)?",
            "f_Franco y Hitler",
            "c_Hitler y Mussolini",
            "f_Pinochet y Mussolini",
            "f_Hitler y Stalin"
        ),
    )
    val preguntasEntretenimiento: MutableList<MutableList<String>> = mutableListOf(
        mutableListOf(
            "¿Cuál de estas películas NO dirigió Steven Spielberg?",
            "f_E.T. el extraterrestre",
            "c_Transformers",
            "f_Parque Jurásico",
            "f_Tiburón"
        ),
        mutableListOf(
            "¿Cómo se llama la danza popular brasileña que parece un combate?",
            "f_Salsa",
            "f_Reggaeton",
            "c_La capoeria",
            "f_Hip hop"
        ),
        mutableListOf(
            "¿Cómo se llama el jabalí  de la pelicula de Disney 'El Rey León'?",
            "f_Boumba",
            "f_Puma",
            "f_Kumpa",
            "c_Pumba"
        ),
        mutableListOf(
            "¿En la serie, 'The Big Bang Theory', ¿cual de estos es el nombre de unos de los protagonistas?",
            "f_Elena",
            "f_Laura",
            "c_Sheldon",
            "f_Apu"
        ),
        mutableListOf(
            "¿Cuál es el nombre de Bono, vocalista de la banda de U2?",
            "f_Julian Casablanca",
            "c_Paul David ",
            "f_Roger Waters",
            "f_Freddy Mercurie"
        ),
        mutableListOf(
            "¿Quién es el presentador del concurso televisivo '¡Boom!'?",
            "c_Juanra Bonet",
            "f_Carlos Sobera",
            "f_Jorge Fernández",
            "f_Arturo Valls"
        ),
        mutableListOf(
            "¿Qué actor interpretó el papel de Michael en el film 'El Cazador'?",
            "f_Christopher Walken",
            "c_Robert De Niro",
            "f_John Cazale",
            "f_George Dzundza"
        ),
        mutableListOf(
            "¿Cuál fue la primera videoconsola de la historia?",
            "c_Magnavox Odyssey",
            "f_Atari 2600",
            "f_Nintendo Entertainment System",
            "f_Sega Master System"
        ),
        mutableListOf(
            "¿Cómo se llama el detective interpretado por Harrison Ford en 'Blade Runner'?",
            "f_Bullock",
            "f_Christopher",
            "f_Lauren",
            "c_Deckard"
        ),
        mutableListOf(
            "¿Cuál fue la película animada de Disney número 50?",
            "f_la bella durmiente",
            "f_Moana",
            "f_El libro de la selva",
            "c_Enredados"
        ),
        mutableListOf(
            "¿Qué artista igualó el récord de Michael Jackson con 5 top one del mismo álbum, en Billboard?",
            "f_Rihanna",
            "c_Katy Perry",
            "f_Madonna",
            "f_Britney Spears"
        ),
        mutableListOf(
            "Sigue el título: Marcelino:",
            "c_Pan y vino",
            "f_Ajo y vino",
            "f_Ajo y agua",
            "f_Pan y agua"
        ),
        mutableListOf(
            "¿A quién intentan eliminar las máquinas en la película 'Terminador 2'?",
            "c_John Connor",
            "f_Arnold Connor",
            "f_Jessica Connor",
            "f_Sarah Conor"
        ),
        mutableListOf(
            "¿Cuál era la debilidad del monstruo 'Triki' en Barrio Sésamo?",
            "f_Contar manzanas",
            "f_La cerdita Peggy",
            "f_Vestirse de superhéroe",
            "c_Las galletas"
        ),
        mutableListOf(
            "¿Cómo se llama el padre de  Bart y Lisa Simpson?",
            "f_Ned Patiño",
            "c_Homer Simpson",
            "f_Alex Simpson",
            "f_Marge Simpson"
        )
    )


}

