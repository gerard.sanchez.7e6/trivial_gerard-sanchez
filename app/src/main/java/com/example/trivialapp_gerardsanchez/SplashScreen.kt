package com.example.trivialapp_gerardsanchez

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler

import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen

@SuppressLint("CustomSplashScreen")
class SplashScreen : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        val splashScreen = installSplashScreen()
        super.onCreate(savedInstanceState)
        splashScreen.setKeepOnScreenCondition { true }
        setContentView(R.layout.activity_splash_screen)
        Handler().postDelayed({
        val intent = Intent(this, MenuActivity::class.java)
        startActivity(intent)
        finish()
    },2000)}
}
