package com.example.trivialapp_gerardsanchez

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isInvisible
import androidx.fragment.app.Fragment
import com.example.trivialapp_gerardsanchez.CategoryFragment.Companion.category
import com.example.trivialapp_gerardsanchez.GameFragment.Companion.score
import com.example.trivialapp_gerardsanchez.databinding.FragmentCategoryBinding
import com.example.trivialapp_gerardsanchez.databinding.FragmentRankingBinding
import com.example.trivialapp_gerardsanchez.databinding.FragmentResultBinding
import java.io.OutputStreamWriter

class ResultFragment : Fragment() {
    private lateinit var binding: FragmentResultBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentResultBinding.inflate(layoutInflater)
        return binding.root

    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        when (category){
            0 -> binding.userCategory.text = "CIENCIA"
            1 -> binding.userCategory.text = "DEPORTES"
            2 -> binding.userCategory.text = "ARTE"
            3 -> binding.userCategory.text = "GEOGRAFÍA"
            4 -> binding.userCategory.text = "HISTORIA"
            5 -> binding.userCategory.text = "ENTRETENIMIENTO"

        }
            binding.userScore.text = "$score"

        binding.buttonMenu.setOnClickListener {
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, MenuFragment())
                setReorderingAllowed(true)
                commit()
            }
        }
        binding.buttonPlayagain.setOnClickListener {
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, CategoryFragment())
                setReorderingAllowed(true)
                commit()
            }
        }
        binding.buttonRanking.setOnClickListener {
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, RankingFragment())
                commit()
            }
        }

    }
}