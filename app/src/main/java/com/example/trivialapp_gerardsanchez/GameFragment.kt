package com.example.trivialapp_gerardsanchez

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.os.Looper
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.example.trivialapp_gerardsanchez.CategoryFragment.Companion.category


import com.example.trivialapp_gerardsanchez.databinding.FragmentGameBinding
import com.example.trivialapp_gerardsanchez.questions.preguntasArte
import com.example.trivialapp_gerardsanchez.questions.preguntasCiencia
import com.example.trivialapp_gerardsanchez.questions.preguntasDeportes
import com.example.trivialapp_gerardsanchez.questions.preguntasEntretenimiento
import com.example.trivialapp_gerardsanchez.questions.preguntasGeografia
import com.example.trivialapp_gerardsanchez.questions.preguntasHistoria

class GameFragment : Fragment(), View.OnClickListener {
    lateinit var binding: FragmentGameBinding

    companion object {
        var score = 0
        var userRound = 0
         private val categoryList = mutableListOf(
            preguntasCiencia, preguntasDeportes, preguntasArte,
            preguntasGeografia, preguntasHistoria, preguntasEntretenimiento
        )
        var question: List<MutableList<String>> = mutableListOf()
        var randomQuestion = categoryList.shuffled()
        var answer: List<MutableList<String>> = mutableListOf()

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentGameBinding.inflate(layoutInflater)
        randomQuestion = categoryList.shuffled()
        score = 0
        userRound = 0
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        question = mutableListOf()
        answer = mutableListOf()
        when (category) {
            0 -> {
                question = preguntasCiencia.shuffled()
                binding.background.setImageResource(R.drawable.fondo_ciencia)
            }
            1 -> {
                question = preguntasDeportes.shuffled()
                binding.background.setImageResource(R.drawable.fondo_deportes)

            }
            2 -> {
                question = preguntasArte.shuffled()
                binding.background.setImageResource(R.drawable.fondo_arte)

            }
            3 -> {
                question = preguntasGeografia.shuffled()
                binding.background.setImageResource(R.drawable.fondo_geografia)

            }
            4 -> {
                question = preguntasHistoria.shuffled()
                binding.background.setImageResource(R.drawable.fondo_historia)

            }
            5 -> {
                question = preguntasEntretenimiento.shuffled()
                binding.background.setImageResource(R.drawable.fondo_entretenimiento)

            }

        }
        updateData()
        binding.answer1.setOnClickListener(this)
        binding.answer2.setOnClickListener(this)
        binding.answer3.setOnClickListener(this)
        binding.answer4.setOnClickListener(this)

    }

    override fun onClick(v: View?) {
        val button = v as Button
        val tag = button.tag.toString().toInt()
        if (question[userRound][tag].split("_")[0] == "c" || question[userRound][2] == button.text) {

            binding.answer1.isEnabled = false
            binding.answer2.isEnabled = false
            binding.answer3.isEnabled = false
            binding.answer4.isEnabled = false
            score += binding.timerDown.progress
            button.setBackgroundResource(R.drawable.correct_button)
        } else {
            button.setBackgroundResource(R.drawable.incorrect_button)
            correctAnswer()
        }
        timer.cancel()
        delay(1300)
    }

    @SuppressLint("SuspiciousIndentation")
    fun correctAnswer() {

        binding.answer1.isEnabled = false
        binding.answer2.isEnabled = false
        binding.answer3.isEnabled = false
        binding.answer4.isEnabled = false

            if (question[userRound][1].split("_")[0] == "c"){
                binding.answer1.setBackgroundResource(R.drawable.correct_button)
            }
            if (question[userRound][2].split("_")[0] == "c") {
                binding.answer2.setBackgroundResource(R.drawable.correct_button)
            }
            if (question[userRound][3].split("_")[0] == "c"){
                binding.answer3.setBackgroundResource(R.drawable.correct_button)
            }
            if (question[userRound][4].split("_")[0] == "c") {
                binding.answer4.setBackgroundResource(R.drawable.correct_button)
            }
        }


    @SuppressLint("SetTextI18n")
    fun updateData() {
        binding.timerDown.progress = 1000
        binding.timerDown2.progress = 1000

        binding.round.text = "${userRound + 1}/10"

        binding.answer1.text = question[userRound][1].split("_")[1]
        binding.answer2.text = question[userRound][2].split("_")[1]
        binding.answer3.text = question[userRound][3].split("_")[1]
        binding.answer4.text = question[userRound][4].split("_")[1]

        binding.answer1.isEnabled = true
        binding.answer2.isEnabled = true
        binding.answer3.isEnabled = true
        binding.answer4.isEnabled = true

        binding.answer1.setBackgroundResource(R.drawable.button_play)
        binding.answer2.setBackgroundResource(R.drawable.button_play)
        binding.answer3.setBackgroundResource(R.drawable.button_play)
        binding.answer4.setBackgroundResource(R.drawable.button_play)
        binding.question.text = question[userRound][0]
       timer.start()

    }


    fun delay(miliSec: Int) {
        timer.cancel()
        userRound++
        Handler(Looper.getMainLooper()).postDelayed({
            if (userRound == 10 ) {
                changeFragment()
                userRound = 0
            }
            else{
                updateData()
            }

        }, miliSec.toLong())
    }

    private fun changeFragment() {
        timer.cancel()
        userRound = 0
        parentFragmentManager.beginTransaction().apply {
            replace(R.id.fragmentContainerView, ResultFragment())
            commit()
        }
    }
    private val timer = object : CountDownTimer(10250, 100) {
        override fun onTick(millisUntilFinished: Long) {
            binding.timerDown.progress -= 1
            binding.timerDown2.progress -= 1

        }
        override fun onFinish() {
            correctAnswer()
            delay(1300)
        }
    }
}