package com.example.trivialapp_gerardsanchez

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle


class MenuActivity : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)

        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragmentContainerView, MenuFragment())
            commit()
        }

    }
}