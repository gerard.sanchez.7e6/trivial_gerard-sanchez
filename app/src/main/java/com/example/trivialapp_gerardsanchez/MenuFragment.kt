package com.example.trivialapp_gerardsanchez

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import com.example.trivialapp_gerardsanchez.databinding.FragmentMenuBinding


class MenuFragment : Fragment() {
    lateinit var binding:  FragmentMenuBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View{
        binding = FragmentMenuBinding.inflate(layoutInflater)
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.buttonPlay.setOnClickListener{
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, CategoryFragment())
                commit()
            }
        }
        binding.buttonRanking.setOnClickListener{
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView,RankingFragment())
                commit()
            }

        }

        binding.buttonHelp.setOnClickListener {
            val alertDialog = AlertDialog.Builder(requireContext()).create()
            alertDialog.setTitle("COMO SE JUEGA?")
            alertDialog.setMessage("En cada partida debes responder a 10 preguntas de la temática seleccionada. A mayor velocidad, mayor puntuación obtendrás al acertar...\n\nDale a jugar para empezar!!!")
            alertDialog.setButton(
                AlertDialog.BUTTON_NEUTRAL, "SALIR"
            ) { dialog, _ -> dialog.dismiss() }
            alertDialog.show()
        }

    }


}